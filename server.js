//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser');
app.use(bodyParser.json())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-with, Content-Type, Accept");
  next();
});

var requestjson=require('request-json');
var path = require('path');
var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/nmoncada/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var ClientesMLab = requestjson.createClient(urlmovimientosMLab);

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function (req,res){
  res.sendFile(path.join(__dirname, 'index.html'));
});

//app.get('/clientes/:idcliente/:nombre', function(req,res){
//  res.send("aqui tiene al cliente numero: " + req.params.idcliente + " su nombre es " + req.params.nombre);
app.get('/clientes/:idcliente/', function(req,res){
  res.send("aqui tiene al cliente numero: " + req.params.idcliente);

});

var movimientosJSON = require('./movimientosv1.json');
app.get('/v1/movimientos', function(req,res){
  res.send(movimientosJSON);
});

app.post('/', function (req,res){
  res.send("Hemos recibido su peticion post");
});

app.put('/', function (req,res){
  res.send("Hemos recibido su peticion put cambiada");
});

app.delete('/', function (req,res){
  res.send("Hemos recibido su peticion delete");
});

app.get('/movimientos', function(req,res){
  ClientesMLab.get('',function(err,resM,body) {
    if (err) {
      console.log(body);
    } else {
      res.send(body);
    }
  })
})

app.post('/movimientos', function(req,res){
  ClientesMLab.post('', req.body, function(err,resM,body) {
      res.send(body);
  })
})
